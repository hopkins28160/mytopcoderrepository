# README #

This repository will contain code and Jupyter notebooks for Topcoder THRIVE posts in 2020. There will be a series of posts dealing with
exploratory data analysis, game development with pygame and arcade, and R-related topics. Readers can go to the repository, clone it, and
modifiy it to reinforce the concepts presented in the blog post.